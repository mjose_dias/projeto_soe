# PROJETO_SOE



## Instalar o OpenCV

```
sudo apt-get update
sudo apt-get install libopencv-dev python3-opencv
```
## Funcionamento

Faces.cpp utiliza arquivos de fotos e detecta face e os olhos, quando os olhos estão fechados não são detectados.

videos.cpp utiliza arquivos de video e detecta face e os olhos, quando os olhos estão fechados não são detectados.

webcamlive.cpp utiliza imagens diretamente da webcam e detecta face e os olhos, quando os olhos estão fechados não são detectados.

detectaolhos.cpp utiliza imagens diretamente da webcam e detecta face e os olhos, quando os olhos estão fechados não são detectados.Quando os olhos não são detectados, aparece um quadrado rosa no canto superior esquerdo.(em um futura versão irá aparecer um circulo amarelo quando o rosto não for detectado)

## Compilar os códigos


```
g++ Faces.cpp -o Faces `pkg-config --cflags --libs opencv`
g++ videos.cpp -o videos `pkg-config --cflags --libs opencv`
g++ webcamlive.cpp -o wclive `pkg-config --cflags --libs opencv`
g++ detectaolhos.cpp -o olhos `pkg-config --cflags --libs opencv`
```

## Baixar os cascades

```
wget https://raw.githubusercontent.com/DiogoCaetanoGarcia/Sistemas_Embarcados/master/4_Dicas/4.2_OpenCV/Cascades/haarcascade_eye_tree_eyeglasses.xml

wget https://raw.githubusercontent.com/DiogoCaetanoGarcia/Sistemas_Embarcados/master/4_Dicas/4.2_OpenCV/Cascades/haarcascade_frontalface_alt.xml
```


## Utilização dos códigos

```
./Faces imagem cascade_da_face cascade_do_olho
./videos video cascade_da_face cascade_do_olho
./wclive cascade_da_face cascade_do_olho
./olhos cascade_da_face cascade_do_olho
```

