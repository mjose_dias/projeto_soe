#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
#include <chrono>
#include <queue>

using namespace std;
using namespace cv;

/** Function Headers */
int detectAndDisplay(Mat frame, int& ROSTO);
int rising_edge( int& atual, int& passado);

/** Global variables */
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

/** @function main */
int main(int argc, const char** argv)
{
    //-- 1. Load the cascades
    if (!face_cascade.load(argv[1]))
    {
        cout << "--(!)Error loading face cascade\n";
        return -1;
    }
    if (!eyes_cascade.load(argv[2]))
    {
        cout << "--(!)Error loading eyes cascade\n";
        return -1;
    }

    VideoCapture capture(0);
    if (!capture.isOpened())
    {
        cout << "--(!)Error opening camera\n";
        return -1;
    }

	capture.set(CAP_PROP_FRAME_WIDTH, 320); // Definir largura desejada
	capture.set(CAP_PROP_FRAME_HEIGHT, 240); // Definir altura desejada

	int ROSTO = 0;
	int OLHO = 0;
	int valorpassado=0;
	int borda=0;

    Mat frame;
    auto start_time = chrono::steady_clock::now();

    // Fila para armazenar os últimos 10 valores de OLHO
    queue<int> olhoQueue;
    int olhoSum = 0;

    while (true)
    {
        capture >> frame;

        if (frame.empty())
        {
            cout << "--(!) No captured frame -- Break!\n";
            break;
        }

        OLHO = detectAndDisplay(frame, ROSTO);

        // Adicionar valor atual de OLHO à fila
        olhoQueue.push(OLHO);
        olhoSum += OLHO;

        // Verificar se a fila está com mais de 10 elementos
        if (olhoQueue.size() > 10)
        {
            // Remover o valor mais antigo da fila
            olhoSum -= olhoQueue.front();
            olhoQueue.pop();
        }

        // Calcular a média móvel dos últimos 10 valores de OLHO
  //      float olhoMediaMovel = (float) olhoSum / olhoQueue.size();
        int buzzer = 0;

        auto current_time = chrono::steady_clock::now();
        auto elapsed_time = chrono::duration_cast<chrono::milliseconds>(current_time - start_time).count();
        if (elapsed_time >= 100)
        {
            // cout << "Média móvel de OLHO: " << olhoMediaMovel << endl;
            if (olhoSum <= 4)
            {
                buzzer = 1;
            }
            else
            {
                buzzer = 0;
            }
            //cout << "buzzer: " << buzzer << endl;
            start_time = current_time;
	    borda=rising_edge(buzzer,valorpassado);
	    valorpassado=buzzer;
	    //cout << "borda: " << borda << endl;
        }

       // imshow("Capture - Face detection", frame);

        if (waitKey(100) == 27) // Alterado o valor de espera para 100 ms (10 fps)
        {
            cout << "Esc key is pressed by the user. Stopping the video...\n";
            break;
        }
    }

    return 0;
}

/** @function detectAndDisplay */
int detectAndDisplay(Mat frame, int& ROSTO)
{
    int OLHO = 0;

    Mat frame_gray;
    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);

    //-- Detect faces
    std::vector<Rect> faces;
    face_cascade.detectMultiScale(frame_gray, faces);

    if (faces.empty())
    {
        ROSTO = 0;
        OLHO = 1;
        // Desenhe um quadrado amarelo no canto inferior esquerdo da imagem
        //Rect square(frame.cols - 110, frame.rows - 110, 100, 100);
        //rectangle(frame, square, Scalar(0, 255, 255), 4);
    }
    else
    {
        ROSTO = 1;
        for (size_t i = 0; i < faces.size(); i++)
        {
            Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
            //ellipse(frame, center, Size(faces[i].width / 2, faces[i].height / 2), 0, 0, 360, Scalar(255, 0, 255), 4);

            Mat faceROI = frame_gray(faces[i]);

            //-- In each face, detect eyes
            std::vector<Rect> eyes;
            eyes_cascade.detectMultiScale(faceROI, eyes);

            if (eyes.empty())
            {
                OLHO = 0;
                // Desenhe um quadrado vermelho no canto superior esquerdo da imagem
                // Rect square(10, 10, 100, 100);
                // rectangle(frame, square, Scalar(0, 0, 255), 4);
            }
            else
            {
                OLHO = 1;
                for (size_t j = 0; j < eyes.size(); j++)
                {
                    Point eye_center(faces[i].x + eyes[j].x + eyes[j].width / 2, faces[i].y + eyes[j].y + eyes[j].height / 2);
                    int radius = cvRound((eyes[j].width + eyes[j].height) * 0.25);
                    // circle(frame, eye_center, radius, Scalar(255, 0, 0), 4);
                }
            }
        }
    }

    return OLHO;
}



//função para detecção de borda

int rising_edge( int& atual, int& passado)
{

int borda;

if (passado==0 && atual==1)
	{
		borda=1;
		system("date");
	}
else
	{
		borda=0;
	}
return borda;
}





