#include <iostream>

int main() {
    bool bitAnterior; // Armazena o valor anterior do bit
    bool bitAtual; // Armazena o valor atual do bit

    // Solicita o primeiro bit
    std::cout << "Digite o primeiro bit (0 ou 1): ";
    std::cin >> bitAnterior;

    // Loop para receber continuamente novos bits
    while (true) {
        std::cout << "Digite um novo bit (0 ou 1): ";
        std::cin >> bitAtual;

        // Verifica se houve uma borda de subida (0 para 1)
        if (!bitAnterior && bitAtual) {
            std::cout << "Borda de subida detectada!" << std::endl;
        }
        // Verifica se houve uma borda de descida (1 para 0)
        else if (bitAnterior && !bitAtual) {
            std::cout << "Borda de descida detectada!" << std::endl;
        }

        bitAnterior = bitAtual; // Atualiza o valor anterior do bit
    }

    return 0;
}

