#include <stdio.h>
#include <unistd.h>
#include <time.h>

int main()
{
    float face, olho;
    int contador = 0;
    float media = 0.0;
    int buzzer = 0;

    while (true)
    {
        // Simulação da coleta das variáveis face e olho
        // Substitua esse trecho pelo código real para receber as variáveis do ambiente

        // Exemplo de valores para teste
        face = 1.0;
        olho = 0.0;

        // Atualizar o contador e calcular a média
        contador++;
        media += olho;

        if (contador == 10)
        {
            // Verificar as condições e atualizar o valor do buzzer
            if (face == 0)
            {
                buzzer = 0;
            }
            else if (face == 1 && olho == 1)
            {
                buzzer = 0;
            }
            else if (face == 1 && olho == 0)
            {
                media /= contador;

                if (media <= 0.3)
                {
                    buzzer = 1;
                }
                else
                {
                    buzzer = 0;
                }
            }

            // Reinicializar o contador e a média
            contador = 0;
            media = 0.0;
        }

        // Exibir o valor do buzzer
        printf("Buzzer: %d\n", buzzer);

        // Aguardar 0.1 segundo (10 vezes por segundo)
        usleep(100000);
    }

    return 0;
}

